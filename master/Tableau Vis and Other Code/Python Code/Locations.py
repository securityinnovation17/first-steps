# Jose Alvarez
# SI 330 Final Project:
# Creating an interactive map using two data sources:
# (1) Wikipedia Infobox Data (Locations) and
# (2) Billboard Top 100 Artists of All Time page (Names)

#Finished in Excel (importing values from text files, concatenating data, etc.)

import wptools
from urllib.request import urlopen
from bs4 import BeautifulSoup
from geopy.geocoders import Nominatim
import os
import urllib

# # This function opens the Billboard Top 100 Page, reads in the HTML code, then pulls only the artists' names using BeautifulSoup
# # It then creates a list of artist names from this, saving it in the variable artist_list
# def create_artist_list():
#     with urllib.request.urlopen('http://www.billboard.com/charts/greatest-hot-100-artists') as response:
#         html_doc = response.read()
#         html = html_doc.decode('utf8')
#
#     soup = BeautifulSoup(html, "html.parser")
#     artist_rows = soup.find_all(class_="chart-row__song")
#
#
#     artist_list= []
#     for row in artist_rows:
#         artist= row.contents[0]
#         artist_list.append(artist.title())
#
#     return artist_list
#
# # This function creates  file with the top 100 names in it, which we use for reference in the next function
# def make_artist_file(artists):
#     if os.path.exists('top100.txt'):
#         return
#
#     top100 = open("top100.txt", 'w')
#     for item in artists:
#         top100.write("%s\n" % item)
#
# # This function reads in the names of each artists, looks up their locations (born or where the band formed, and
# # creates a list of locactions for each artists. Some values could not be obtained through the API so they were added manually (Only about 5 or so)
# def make_loc_file():
#     if os.path.exists('locations.txt'):
#         return
#
#     artist_100 = open("top100.txt", 'r')
#     # new_100= artist_100.readlines()
#
#     # for line in artist_100:
#     #     print (line.strip())
#
#     locations = []
#     for art in artist_100:
#         so = wptools.page(art.strip()).get_parse()
#
#         try:
#             origin = so.infobox['origin']
#             locations.append(origin)
#         except KeyError:
#             birth = so.infobox['birth_place']
#             locations.append(birth)
#
#         except:
#             locations.append("Not Found")
#
#      #This cleans the location data replaces all extraneous words in the locations
#     loc100 = open("locations.txt", 'w')
#     for loc in locations:
#         loc = loc.replace("]", "")
#         loc = loc.replace("[", "")
#         loc = loc.replace("{", "")
#         loc = loc.replace("}", "")
#         loc = loc.replace("nowrap|", "")
#         loc = loc.replace("no wrap|", "")
#         loc = loc.strip()
#         loc100.write("%s\n" % loc)
#     loc100.close()
#
#
#
# # Function calls
# make_list= create_artist_list()
# make_art= make_artist_file(make_list)
# make_loc=make_loc_file()


#This section obtains all latitudes and longitudes for each artist
europe= open("Europe.txt", 'r')
asia= open("Asia.txt", 'r')
namerica= open("North America.txt", 'r')
samerica= open("South America.txt", 'r')

count=2
for line in europe:
    geolocator = Nominatim()
    # try:
    location = geolocator.geocode(line, timeout= None)
    if location != None:
    # print(location.address)
    # Flatiron Building, 175, 5th Avenue, Flatiron, New York, NYC, New York, ...
        print(location.latitude, location.longitude)
    else:
        print(line, "Not found")
    # except:
    #     print(line, " Not found")

    count+=1
# (40.7410861, -73.9896297241625)
# print(location.raw)

europe.close()



# ---------------This section consists of non-working code written during this project (AKA Code Graveyard)-------------------------------

# geolocator = Nominatim()
# location = geolocator.geocode("Manchester, England")
# # print(location.address)
# # Flatiron Building, 175, 5th Avenue, Flatiron, New York, NYC, New York, ...
# print((location.latitude, location.longitude))
# # (40.7410861, -73.9896297241625)
# # print(location.raw)

    # re_loc= re.search(r'\W*(\w*)\w*', line)
    #
    # if re_loc != None:
    #     clean_loc.append(re_loc.group(1))
    #
    # else:
    #     clean_loc.append(line + " [CHECK]")

# for line in loc_test:
#     print (line)
# print (locations)

# so = wptools.page("Kool & The Gang").get_parse()
# origin = so.infobox['origin']
# print (origin)
# art_loc= []
# art_loc.append(artist_list)
# print (type(artist_list))
# origin_list=[]

# for artist in artist_list:
#     so = wptools.page(artist).get_parse()
#
#     # try:
#     #     origin= so.infobox['origin']
#     # except:
#     #     origin= so.infobox['born']
#
# print (origin)
# clean_origin = re.search(r'(<a href="/title/)([\w]+)/.*">([^<]+)</a>', str(origin))

