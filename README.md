# README #

###Please find our documentation here: https://docs.google.com/document/d/1QJuu2Q4bNSQwaTC7FA73KTrL9lUClgln72sMDPshQUM/edit?usp=sharing ###

We aim to create privacy mapping for the communications between vehicles in different cities; This will be done by analysis of topologies to 
deduce privacy levels in the information being sent and received by the vehicle. 

Specifically, we wish to aid Security Innovations in their development of a viable, fluid digital signature that may be assigned different 
pseudonyms based on factors that are constantly changing-- to include road topolgy among other things.

Context-adaptive digital signature based on a short-term identifier and an installed beacon (required on all cars by 2020).

### What is this repository for? ###

* Project Development (still in Beta)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact